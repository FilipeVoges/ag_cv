<?php

/**
 * Generates a random number between 0 and 1
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 0.1
 *
 * @return Float
 */
function random(){
    return (float)rand()/(float)getrandmax();
}

/**
 * calculates the distance in Km between two points
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 0.1
 *
 * @param $coord1 \Coordinate
 * @param $coord2 \Coordinate
 * @return Float
 */
function distance($coord1, $coord2) {

    $x = $coord2->get('x') - $coord1->get('x');
    $y = $coord2->get('y') - $coord1->get('y');

    $dist = 2 * asin(
        sqrt(
            pow(sin($x / 2), 2)
            + cos($coord1->get('x'))
            * cos($coord2->get('x'))
            * pow(sin($y / 2), 2)
        )
    );
    $dist = $dist * 6371;
    return number_format($dist, 2, '.', '');
}
