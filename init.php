<?php

/**
 * Path
 */
define('BASE_PATH', dirname(__FILE__));

/**
 * Debug
 *
 * @example true : Dev Mod
 * @example false : Prod Mod
 */
define('VP_DEBUG', true);

/**
 * Init Set By Debug
 */
if(VP_DEBUG){
    ini_set('display_errors', true);
    error_reporting(E_ALL);
}
