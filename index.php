<?php

use App\Model\Entity\City;
use App\Model\Entity\Population;
use App\Model\Entity\Coordinate;
use App\Model\TourManager;
use App\Model\GA;

require 'vendor/autoload.php';
require 'init.php';

$city = new City("Lisboa", new Coordinate(60, 200));
TourManager::addCity($city);
$city2 = new City("Santarém", new Coordinate(180, 200));
TourManager::addCity($city2);
$city3 = new City("Braga", new Coordinate(80, 180));
TourManager::addCity($city3);
$city4 = new City("Bragança", new Coordinate(140, 180));
TourManager::addCity($city4);
$city5 = new City("Porto", new Coordinate(20, 160));
TourManager::addCity($city5);
$city6 = new City("Portalegre", new Coordinate(100, 160));
TourManager::addCity($city6);
$city7 = new City("Viseu", new Coordinate(200, 160));
TourManager::addCity($city7);
$city8 = new City("Setúbal", new Coordinate(140, 140));
TourManager::addCity($city8);
$city9 = new City("Samora", new Coordinate(40, 120));
TourManager::addCity($city9);
$city10 = new City("Beja", new Coordinate(100, 120));
TourManager::addCity($city10);
$city11 = new City("Aveiro", new Coordinate(180, 100));
TourManager::addCity($city11);
$city12 = new City("Barcelos", new Coordinate(60, 80));
TourManager::addCity($city12);
$city13 = new City("Faro", new Coordinate(120, 80));
TourManager::addCity($city13);
$city14 = new City("Guimarães", new Coordinate(180, 60));
TourManager::addCity($city14);
$city15 = new City("Leiria", new Coordinate(20, 40));
TourManager::addCity($city15);
$city16 = new City("Odivelas", new Coordinate(100, 40));
TourManager::addCity($city16);
$city17 = new City("Sines", new Coordinate(200, 40));
TourManager::addCity($city17);
$city18 = new City("Tonela", new Coordinate(200, 20));
TourManager::addCity($city18);
$city19 = new City("Coimbra", new Coordinate(60, 20));
TourManager::addCity($city19);
$city20 = new City("Almada", new Coordinate(160, 20));
TourManager::addCity($city20);
$city21 = new City("Cascais", new Coordinate(150, 55));
TourManager::addCity($city21);
$city22 = new City("Almeirim", new Coordinate(10, 20));
TourManager::addCity($city22);
$city23 = new City("Chaves", new Coordinate(25, 85));
TourManager::addCity($city23);
$city24 = new City("CR7", new Coordinate(185, 35));
TourManager::addCity($city24);
$city25 = new City("Maia", new Coordinate(100, 95));
TourManager::addCity($city25);

$pop = new Population(50, true);

echo '<strong>Distância inicial: ' . (int)$pop->getFittest()->getDistance() . 'Km</strong><br>';

for($i = 0; $i < 200; $i++) {
    $pop = GA::evolvePopulation($pop);
}

echo '<strong>Distância Final: ' . (int)$pop->getFittest()->getDistance() . 'Km</strong><br>';

echo 'Qunatidade de Mutações: ' . GA::getMutations() . '<br>';

echo 'Solução<br>';

echo '<pre>';
var_dump($pop->getFittest());
echo '</pre>';
die();
