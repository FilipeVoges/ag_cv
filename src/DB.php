<?php

namespace app;

use App\Model\Exception\DatabaseException;

/**
 * connection to the database
 *
 * @author Filipe Voges <filipe.voges@vapps.com.br>
 * @package App
 * @since 1.0
 */
class DB extends \PDO
{
    public function __construct($dsn = null, $username = null, $password = null, $options = array())
    {
        $dsn = ($dsn != null) ? $dsn : sprintf('mysql:dbname=%s;host=%s', MYSQL_DBNAME, MYSQL_HOST);
        $username = ($username != null) ? $username : MYSQL_USER;
        $password = ($password != null) ? $password : MYSQL_PASS;

        parent::__construct($dsn, $username, $password, $options);
        $this->exec('SET NAMES utf8');

    }

    /**
     * Execute a Query
     *
     * @param $query | String
     * @return Boolean
     */
    public function run($query){
        return $this->execute($query);
    }

    /**
     * Execute a Query
     *
     * @param $query | String
     * @param $mul | Mixed
     * @return Mixed
     */
    private function execute($query, $mul = null){
        try{
            $stmt = $this->prepare($query);
            $stats = $stmt->execute();
            if($stats){
                if($mul){
                    $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                }else if(checkType('NUL', $mul)){
                    $result = $stats;
                }else{
                    $result = $stmt->fetch(\PDO::FETCH_ASSOC);
                }
            }else{
                throw new DatabaseException(null, 400, $query);
            }
            return $result;
        }catch(\Exception $e){
            throw new DatabaseException($e->getMessage(), $e->getCode(), $query);
        }
    }

    /**
     * Returns the data of a mysql query
     *
     * @param $from | String
     * @param $multiple | boolean
     * @param $cols | array
     * @param $join | array
     * @param $where | array
     * @param $customFields | array
     *
     * @return Mixed
     */
    public function select($from, $multiple = true, $cols = array('*'), $join = null, $where = null, $customFields = null){
        $query = "SELECT ";

        if(checktype('ARR', $cols)){
            $query .= implode(', ', $cols);
        }else{
            throw new DatabaseException(null, 400, $query);
        }

        if(checktype('STR', $from)){
            $query .= " FROM " . $from;
        }else{
            throw new DatabaseException(null, 400, $query);
        }

        if(!(checktype('NUL', $join))){
            if(checktype('ARR', $join)){
                foreach($join as $j){
                    $query .= ' ';
                    if(isset($j['type'])){
                        $query .= strtoupper($j['type']) . ' JOIN ';
                    }else{
                        throw new DatabaseException(null, 400, $query);
                    }
                    if(isset($j['table'])){
                        $query .= $j['table'];
                    }else{
                        throw new DatabaseException(null, 400, $query);
                    }
                    if(isset($j['on'])){
                        $query .= ' ON ' . $j['on'];
                    }else{
                        throw new DatabaseException(null, 400, $query);
                    }
                }
            }else{
                throw new DatabaseException(null, 400, $query);
            }
        }

        if(!(checktype('NUL', $where))){
            $query .= ' WHERE ';
            if(checktype('ARR', $where)){
                if(isset($where['and']) && !checktype('NUL', $where['and']) && checktype('ARR', $where['and'])){
                    $query .= implode(' AND ', $where['and']);
                    $query .= ' ';
                }
                if(isset($where['or']) && !checktype('NUL', $where['or']) && checktype('ARR', $where['or'])){
                    $query .= implode(' OR ', $where['or']);
                    $query .= ' ';
                }
            }else{
                throw new DatabaseException(null, 400, $query);
            }
        }

        if(!(checktype('NUL', $customFields))){
            $query .= ' ';
            if(checktype('ARR', $customFields)){
                foreach($customFields as $field){
                    $query .= $field . ' ';
                }
            }else{
                throw new DatabaseException(null, 400, $query);
            }
        }

        $res = $this->execute($query, $multiple);

        return $res;
    }

    /**
     * Inserts a Entity in Database
     *
     * @param $table | String
     * @param $entity \Entity
     * @return Mixed
     */
    public function insert($table, $entity){
        $query = 'INSERT INTO ' . $table . '(';

        $attributes = $entity->getAttibutes();

        if(isset($attributes['id'])){
            unset($attributes['id']);
        }

        $cols = array_keys($attributes);
        $cols = implode(', ', $cols);

        $query .= $cols . ') VALUES(';

        $attrs = array();
        foreach($attributes as $attr){
            if(checkType('NUM', $attr)){
                array_push($attrs, $attr);
                continue;
            }
            if(checkType('STR', $attr)){
                $attr = "'$attr'";
                array_push($attrs, $attr);
                continue;
            }
            if(checkType('NUL', $attr)){
                $attr = 'NULL';
                array_push($attrs, $attr);
                continue;
            }
            if($attr instanceof \DateTime){
                $attr = 'CURRENT_TIMESTAMP';
                array_push($attrs, $attr);
                continue;
            }
        }

        $values = implode(', ', $attrs);

        $query .= $values . ');';

        $res = $this->execute($query);

        return $res;
    }

    /**
     * Update a Row
     *
     * @param $table | String
     * @param $values | array
     * @param $where | array
     * @return Mixed
     */
    public function update($table, $values, $where){
        $query = "UPDATE " . $table . ' SET ';

        $values = implode(', ', $values);
        $query .= $values;

        $query .= ' ';

        if(!(checktype('NUL', $where))){
            $query .= ' WHERE ';
            if(checktype('ARR', $where)){
                if(isset($where['and']) && !checktype('NUL', $where['and']) && checktype('ARR', $where['and'])){
                    $query .= implode(' AND ', $where['and']);
                    $query .= ' ';
                }
                if(isset($where['or']) && !checktype('NUL', $where['or']) && checktype('ARR', $where['or'])){
                    $query .= implode(' OR ', $where['or']);
                    $query .= ' ';
                }
            }else{
                throw new DatabaseException(null, 400, $query);
            }
        }

        $query .= 'LIMIT 1;';

        $res = $this->execute($query);

        return $res;
    }

    /**
     * Create a Table
     *
     * @param $tblName | String
     * @param $cols | Array
     * @return Boolean
     */
    public function create($tblName, $cols, $pk){
        $query = "CREATE TABLE `" . $tblName . "` (";

        foreach($cols as $col){
            $query .= implode(' ', $col) . ', ';
        }

        $query .= "PRIMARY KEY ($pk)";

        $query .= ") ENGINE=InnoDB AUTO_INCREMENT=0;";
        
        return $this->execute($query);
    }

    /**
     * Drop a Table
     *
     * @param $tblName | String
     * @return Boolean
     */
    public function drop($tblName){
        $query = "DROP TABLE IF EXISTS $tblName;";

        return $this->execute($query);
    }
}
