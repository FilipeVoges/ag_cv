<?php

namespace App\Model;

use App\Model\Entity\City;

if(!defined('BASE_PATH')) exit;
/**
 * TourManager
 *
 * @package App\Model
 * @since 0.1
 * @author Filipe Voges <filipe.voges@vapps.com.br>
 */
class TourManager{

    /**
     * @var Array
     */
    private static $destinationCities = array();

    /**
     * Add a City
     *
     * @param $city \City
     * @return Void
     */
    public static function addCity(City $city) {
        self::$destinationCities[] = $city;
    }

    /**
     * Returns a City by index
     *
     * @param $index Integer
     * @return \City
     */
    public static function getCity($index){
        return self::$destinationCities[$index];
    }

    /**
     * Return the total of Cities in List
     *
     * @return Integer
     */
    public static function numberOfCities(){
        return sizeof(self::$destinationCities);
    }
}
