<?php

namespace App\Model\Entity;

use App\Model\Exception\ResourceException;

if(!defined('BASE_PATH')) exit;
/**
 * City
 *
 * @package App\Model\Entity
 * @since 0.1
 * @author Filipe Voges <filipe.voges@vapps.com.br>
 */
class City extends Entity {

    /**
     * @var String
     */
    protected $name;

    /**
     * @var \Coordinate
     */
    protected $coord;

    /**
     * Construct Class
     *
     * @param $name String
     * @param $coord \Coordinate
     */
    public function __construct($name, $coord){
        $this->name = $name;
        $this->coord = $coord;
    }

    /**
     * Calculates the distance to a city
     *
     * @param $city \City
     * @return Float
     */
    public function distanceTo(City $city){
        $coordCity = $city->get('coord');
        $xDif = abs($this->coord->get('x') - $coordCity->get('x'));
        $yDif = abs($this->coord->get('y') - $coordCity->get('y'));
        $distance = sqrt(pow($xDif, 2) + pow($yDif, 2));
        return $distance;
    }

    /**
     * Checks if one city is equal to another city
     *
     * @param $city \City
     * @return Boolean
     */
    public function equals(City $city){
        $city2 = null;
    	if($city instanceof City){
    		$city2 = $city;
    	}else{
    		return false;
    	}

        if($this->coord->get('y') == $city2->get('coord')->get('y')){
            if($this->coord->get('x') == $city2->get('coord')->get('x')){
                return true;
            }
        }
        return false;
    }
}
