<?php

namespace App\Model\Entity;

use App\Model\Exception\ResourceException;

if(!defined('BASE_PATH')) exit;
/**
 * Population
 *
 * @package App\Model\Entity
 * @since 0.1
 * @author Filipe Voges <filipe.voges@vapps.com.br>
 */
class Population extends Entity {

    /**
     * @var Array
     */
    protected $tours;

    /**
     * Construct Class
     *
     * @param $populationSize Integer
     * @param $initialise Boolean
     */
    public function __construct($populationSize, $initialise) {
        $this->tours = array();
        if($initialise){
            for ($i = 0; $i < $populationSize; $i++) {
                $newTour = new Tour();
                $newTour->generateRandomTour();
                $this->saveTour($i, $newTour);
            }
        }else{
            for ($i = 0; $i < $populationSize; $i++) {
                $this->saveTour($i, null);
            }
        }
    }

    /**
     * Saves a route from the route array
     *
     * @param $index Integer
     * @param $tour \Tour
     * @return Void
     */
    public function saveTour($index, $tour) {
        $this->tours[$index] = $tour;
    }

    /**
     * Returns a route of this population
     *
     * @param $index Integer
     * @return \Tour
     */
    public function getTour($index) {
        return $this->tours[$index];
    }

    /**
     * Returns the best tour of this population
     *
     * @return \Tour
     */
    public function getFittest() {
        $fittest = $this->getTour(0);

        for($i = 1; $i < $this->populationSize(); $i++) {
            if($fittest->getFitness() <= $this->getTour($i)->getFitness()) {
                $fittest = $this->getTour($i);
            }
        }
        return $fittest;
    }

    /**
     * Returns population size
     *
     * @return Integer
     */
    public function populationSize() {
        return sizeof($this->tours);
    }
}
