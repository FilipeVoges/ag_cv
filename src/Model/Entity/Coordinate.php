<?php

namespace App\Model\Entity;

use App\Model\Exception\ResourceException;

if(!defined('BASE_PATH')) exit;
/**
 * Coordinate
 *
 * @package App\Model\Entity
 * @since 0.1
 * @author Filipe Voges <filipe.voges@vapps.com.br>
 */
class Coordinate extends Entity {

    /**
     * @var Integer
     */
    protected $x;

    /**
     * @var Integer
     */
    protected $y;

    /**
     * Construct Class
     *
     * @param $x Integer
     * @param $y Integer
     */
    public function __construct($x, $y){
        $this->x = deg2rad($x);
        $this->y = deg2rad($y);
    }
}
