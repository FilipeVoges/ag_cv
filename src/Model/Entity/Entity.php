<?php

namespace App\Model\Entity;

use App\Model\Exception\ResourceException;

if(!defined('BASE_PATH')) exit;
/**
 * Entity
 *
 * @package App\Model\Entity
 * @since 0.1
 * @author Filipe Voges <filipe.voges@vapps.com.br>
 */
abstract class Entity {

    /**
     * Getter Generic
     *
     * @param $property | String
     * @return mixed
     */
    public function get($property){
        if(property_exists($this, $property)){
            return $this->$property;
        }else{
            throw new ResourceException('', 500, get_class($this));
        }
    }

    /**
     * Setter Generic
     *
     * @param $property | String
     * @param $value | mixed
     * @return $this
     */
    public function set($property, $value){
        if(property_exists($this, $property)){
            $this->$property = $value;
        }else{
            throw new ResourceException('', 500, get_class($this));
        }

        return $this;
    }

    /**
     * Returns a Attributes of Class
     *
     * @return Array
     */
    public function getAttibutes(){
        return get_object_vars($this);
    }
}
