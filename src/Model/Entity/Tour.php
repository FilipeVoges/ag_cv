<?php

namespace App\Model\Entity;

use App\Model\Exception\ResourceException;
use App\Model\TourManager;

if(!defined('BASE_PATH')) exit;
/**
 * Tour
 *
 * @package App\Model\Entity
 * @since 0.1
 * @author Filipe Voges <filipe.voges@vapps.com.br>
 */
class Tour extends Entity {

    /**
     * @var Array
     */
    protected $tour;

    /**
     * @var Float
     */
    protected $fitness;

    /**
     * @var Float
     */
    protected $distance;

    /**
     * Construct Class
     *
     * @param $tour Array
     */
    public function __construct($tour = []){
        if(empty($tour)){
            for($i = 0; $i < TourManager::numberOfCities(); $i++){
                $this->tour[] = null;
            }
        }else{
            $this->tour = $tour;
        }
    }

    /**
     * Generate a Random Tour
     *
     * @return Void
     */
    public function generateRandomTour(){
        $numCities = TourManager::numberOfCities();
        for($cityIndex = 0; $cityIndex < $numCities; $cityIndex++) {
        	$this->setCity($cityIndex, TourManager::getCity($cityIndex));
        }
        shuffle($this->tour);
    }

    /**
     * Returns a City by index
     *
     * @param $index Integer
     * @return \City
     */
    public function getCity($index){
        return $this->tour[$index];
    }

    /**
     * Define a City to a index
     *
     * @param $pos Integer
     * @param $city \City
     * @return Void
     */
    public function setCity($pos, City $city) {
        $this->tour[$pos] = $city;

        $this->fitness = 0;
        $this->distance = 0;
    }

    /**
     * Calculate the Fitness of Tour
     *
     * @return Float
     */
    public function getFitness(){
        if($this->fitness == 0) {
        	$this->fitness = 1/$this->getDistance();
        }
        return $this->fitness;
    }

    /**
     * Calculate the total length of the Tour
     *
     * @return Float
     */
    public function getDistance(){
        if($this->distance == 0){
            $tourDistance = 0;

            for($cityIndex=0; $cityIndex < $this->size(); $cityIndex++) {
                $fromCity = $this->getCity($cityIndex);
                $destinationCity = null;
                if($cityIndex+1 < $this->size()){
                    $destinationCity = $this->getCity($cityIndex+1);
                }else{
                    $destinationCity = $this->getCity(0);
                }
                $tourDistance += $fromCity->distanceTo($destinationCity);
            }
            $this->distance = $tourDistance;
        }
        return $this->distance;
    }

    /**
     * Return the total of Cities in Tour
     *
     * @return Integer
     */
    public function size() {
        return sizeof($this->tour);
    }

    /**
     * Checks if the tour already contains a city
     *
     * @param $city \City
     * @return Boolean
     */
    public function containsCity(City $city){
        return in_array($city, $this->tour);
    }

    /**
     * Checks if one tour is equal to another tour
     *
     * @param $tour \Tour
     * @return Boolean
     */
    public function equals(Tour $tour){
    	$tour2 = null;
    	if($tour instanceof Tour){
    		$tour2 = $tour;
    	}else{
    		return false;
    	}
        $tamTour = $this->size();
    	if($tamTour == $tour2->size()){
    		$city1 = null;
            $city2 = null;
    		for($cityIndex = 0; $cityIndex < $tamTour; $cityIndex++){
    			$city1 = $this->getCity($cityIndex);
    			$city2 = $tour2->getCity($cityIndex);
    			if(!$city1->equals($city2)){
    				return false;
    			}
    		}
    		return true;
    	}
    	return false;
    }

}
