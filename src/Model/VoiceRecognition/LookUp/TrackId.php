<?php

namespace AcoustId\LookUp;

use App\Model\VoiceRecognition\LookUp;

if(!defined('BASE_PATH')) exit;
/**
 * \TrackId
 *
 * @package App\Model\VoiceRecognition\LookUp
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 2018
 */
class TrackId extends LookUp
{

    /**
     * @var String
     */
    protected $trackId;

    /**
     * @var Array
     */
    protected $requiredParameters = array(
        'trackId'
    );

    /**
     * Construct Class
     *
     * @param $trackId
     */
    public function __construct($trackId)
    {
        $this->trackId = $trackId;
    }
}
