<?php

namespace App\Model\VoiceRecognition\LookUp;

use App\Model\VoiceRecognition\LookUp;

if(!defined('BASE_PATH')) exit;
/**
 * \FingerPrint
 *
 * @package App\Model\VoiceRecognition\LookUp
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 2018
 */
class FingerPrint extends LookUp
{
    /**
     * @var Integer
     */
    protected $duration;

    /**
     * @var String
     */
    protected $fingerPrint;

    /**
     * @var Array
     */
    protected $requiredParameters = array(
        'duration',
        'fingerPrint'
    );

    /**
     * Construct Class
     *
     * @param $duration
     * @param $fingerPrint
     */
    public function __construct($duration, $fingerPrint)
    {
        $this->duration = $duration;
        $this->fingerPrint = $fingerPrint;
    }

}
