<?php

namespace App\Model\VoiceRecognition;

use App\Model\Entity\Entity;

if(!defined('BASE_PATH')) exit;
/**
 * \ListByMDID
 *
 * @package App\Model\VoiceRecognition
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 2018
 */
class ListByMDID extends Entity
{
    /**
     * @var string
     */
    protected $format = 'json';

    /**
     * @var array
     */
    protected $formatAllowedValues = ['json', 'jsonp', 'xml'];

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var string
     */
    protected $jsonCallBack = 'jsonAcoustidApi';

    /**
     * @var string|array
     */
    protected $mbid;

    /**
     * @var array
     */
    protected $requiredParameters = [
        'mbid',
    ];

    /**
     * @var int
     */
    protected $batch = 0;

    /**
     * @var string
     */
    protected $url = 'http://api.acoustid.org/v2/track/list_by_mbid';

    /**
     * Construct Class
     *
     * @param string $mbid
     */
    public function __construct($mbid){
        $this->setMBID($mbid);
    }

    /**
     * Set a Response Format
     *
     * @param $format string
     *
     * @throws Exception
     * @return $this
     */
    public function setFormat($format = 'json'){
        if(!in_array($format, $this->formatAllowedValues)){
            throw new Exception("Passed $format value is not in list of allowed values. Allowed: " . join(', ', $this->formatAllowedValues));
        }

        if($format == 'jsonp'){
            $this->set('jsonCallBack', 'jsonAcoustidApi');

            $this->format = (string)$format;
        }

        $this->format = (string)$format;

        return $this;
    }

    /**
     * Set MBID
     *
     * @param $mbid  string|array
     * @param Void
     */
    public function setMBID($mbid)
    {
        if(is_array($mbid)){
            $this->set('batch', 1);
        }

        $this->mbid = $mbid;
    }

}
