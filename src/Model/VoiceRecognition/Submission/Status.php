<?php

namespace App\Model\VoiceRecognition\Submission;

use App\Model\Entity\Entity;

if(!defined('BASE_PATH')) exit;
/**
 * \Status
 *
 * @package App\Model\VoiceRecognition\Submission
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 2018
 */
class Status extends Entity
{

    /**
     * @var String
     */
    protected $format = 'json';

    /**
     * @var Array
     */
    private $formatAllowedValues = array(
        'json',
        'xml'
    );

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var string
     */
    protected $clientVersion = '1.0';

    /**
     * @var Integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $url = 'http://api.acoustid.org/v2/submission_status';

    /**
     * @var Array
     */
    protected $requiredParameters = array(
        'id',
    );

    /**
     * Construct Class
     *
     * @param $id Integer
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Set response format
     *
     * @param string $format
     *
     * @throws Exception
     * @return $this
     */
    public function setFormat($format)
    {
        if(!in_array($format, $this->formatAllowedValues)) {
            throw new Exception("Passed $format value is not in list of allowed values. Allowed: " . join(', ', $this->formatAllowedValues));
        }

        $this->format = (string)$format;

        return $this;
    }
}
