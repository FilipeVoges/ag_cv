<?php

namespace App\Model\VoiceRecognition\Submission;

use App\Model\VoiceRecognition\Submission;

if(!defined('BASE_PATH')) exit;
/**
 * \Batch
 *
 * @package App\Model\VoiceRecognition\Submission
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 2018
 */
class Batch extends Submission
{
}
