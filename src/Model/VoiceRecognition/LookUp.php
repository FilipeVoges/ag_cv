<?php

namespace App\Model\VoiceRecognition;

use App\Model\Entity\Entity;

if(!defined('BASE_PATH')) exit;
/**
 * Class LookUp
 *
 * @package App\Model\VoiceRecognition
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 2018
 */
abstract class LookUp extends Entity
{
    /**
     * @var string
     */
    protected $format = 'json';

    /**
     * @var array
     */
    protected $formatAllowedValues = array(
        'json',
        'jsonp',
        'xml'
    );

    /**
     * @var string
     */
    protected $jsonCallBack = 'jsonAcoustidApi';

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var array
     */
    protected $meta;

    /**
     * @var array
     */
    protected $metaAllowedValues = array(
        'recordings',
        'recordingids',
        'releases',
        'releaseids',
        'releasegroups',
        'releasegroupids',
        'tracks',
        'compress',
        'usermeta',
        'sources'
    );

    /**
     * @var string
     */
    protected $url = 'http://api.acoustid.org/v2/lookup';

    /**
     * Set response format
     *
     * @param $format String
     *
     * @throws Exception
     * @return $this
     */
    public function setFormat($format = 'json'){
        if(!in_array($format, $this->formatAllowedValues)) {
            throw new Exception("Passed $format value is not in list of allowed values. Allowed: " . join(', ', $this->formatAllowedValues));
        }

        if($format == 'jsonp'){
            $this->set('jsonCallBack', 'jsonAcoustidApi');
            $this->format = (string)$format;
        }

        $this->format = (string) $format;

        return $this;
    }

    /**
     * Set required meta info.
     *
     * @param $meta Array
     *
     * @throws Exception
     * @return $this
     */
    public function setMeta($meta){
        if(empty($meta) || !is_array($meta)) {
            throw new Exception("$meta parameter must be an array");
        }
        foreach($meta as $item) {
            if(!in_array($item, $this->metaAllowedValues)) {
                throw new Exception("Passed $meta (" . $item . ") member is not in list of allowed values. Allowed: " . join(', ', $this->metaAllowedValues));
            }
            $this->meta[] = (string)$item;
        }

        return $this;
    }

}
