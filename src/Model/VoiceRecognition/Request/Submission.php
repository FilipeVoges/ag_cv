<?php

namespace App\Model\VoiceRecognition\Request;

use App\Model\VoiceRecognition\Request;

if(!defined('BASE_PATH')) exit;
/**
 * \Submission
 *
 * @package App\Model\VoiceRecognition
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 2018
 */
class Submission extends Request
{
    /**
     * Create request url based on $instance
     *
     * @return $this
     */
    public function createRequest(){
        $this->set('url', $this->instance->get('url'));

        $this->params['client'] = $this->instance->get('apiKey');
        $this->params['user'] = $this->instance->get('user');
        $this->params['duration'] = $this->instance->get('duration');
        $this->params['fingerprint'] = $this->instance->get('fingerPrint');

        $this->params['format'] = $this->instance->get('format');
        $this->params['clientversion'] = $this->instance->get('clientversion');
        $this->params['wait'] = $this->instance->get('wait');
        $this->params['bitrate'] = $this->instance->get('bitrate');
        $this->params['fileformat'] = $this->instance->get('fileFormat');
        $this->params['mbid'] = $this->instance->get('mbid');
        $this->params['track'] = $this->instance->get('track');
        $this->params['artist'] = $this->instance->get('artist');
        $this->params['album'] = $this->instance->get('album');
        $this->params['albumartist'] = $this->instance->get('albumArtist');
        $this->params['year'] = $this->instance->get('year');
        $this->params['trackno'] = $this->instance->get('trackNo');
        $this->params['discno'] = $this->instance->get('discNo');

        $this->set('url', $this->get('url') . '?' . urldecode(http_build_query($this->params)));

        return $this;
    }
}
