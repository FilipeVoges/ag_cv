<?php namespace App\Model\VoiceRecognition\Request;

use App\Model\VoiceRecognition\LookUp\FingerPrint;
use App\Model\VoiceRecognition\LookUp\TrackId;
use App\Model\VoiceRecognition\Request;

if(!defined('BASE_PATH')) exit;
/**
 * \LookUp
 *
 * @package App\Model\VoiceRecognition\Request
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 2018
 */
class LookUp extends Request
{
    /**
     * Create request url based on $instance
     *
     * @return $this
     */
    public function createRequest(){

        $this->set('url', $this->instance->get('url'));

        $this->params['client'] = $this->instance->get('apiKey');
        $this->params['format'] = $this->instance->get('format');
        if ($this->params['format'] == 'jsonp') {
            $this->params['jsoncallback'] = $this->instance->get('jsonCallBack');
        }

        if (!empty($this->instance->get('meta'))) {
            $this->params['meta'] = join('+', $this->instance->get('meta'));
        }

        switch(get_class($this->instance)) {
            case FingerPrint::class:
                $this->params['duration']    = $this->instance->get('duration');
                $this->params['fingerprint'] = $this->instance->get('fingerPrint');
                break;

            case TrackId::class:
                $this->params['trackid'] = $this->instance->get('trackId');
                break;
        }

        $this->set('url', $this->get('url') . '?' . urldecode(http_build_query($this->params)));

        return $this;
    }
}
