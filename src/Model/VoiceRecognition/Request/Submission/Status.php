<?php

namespace App\Model\VoiceRecognition\Request\Submission;

use App\Model\VoiceRecognition\Request;

if(!defined('BASE_PATH')) exit;
/**
 * \Status
 *
 * @package App\Model\VoiceRecognition\Request\Submission
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 2018
 */
class Status extends Request
{

    /**
     * Create request url based on $instance
     *
     * @return $this
     */
    public function createRequest()
    {
        $this->set('url', $this->instance->get('url'));

        $this->params['client'] = $this->instance->get('apiKey');
        $this->params['format'] = $this->instance->get('format');
        $this->params['clientversion'] = $this->instance->get('clientVersion');
        $this->params['id'] = $this->instance->get('id');

        $this->set('url', $this->get('url') . '?' . urldecode(http_build_query($this->params)));

        return $this;
    }
}
