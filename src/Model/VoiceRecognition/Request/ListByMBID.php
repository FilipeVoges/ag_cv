<?php namespace App\Model\VoiceRecognition\Request;

use App\Model\VoiceRecognition\ListByMDID;
use App\Model\VoiceRecognition\Request;

if(!defined('BASE_PATH')) exit;
/**
 * \ListByMBID
 *
 * @package App\Model\VoiceRecognition\Request
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 2018
 */
class ListByMBID extends Request
{
    /**
     * Create request url based on $instance
     *
     * @return $this
     */
    public function createRequest(){
        $x = '';

        $this->set('url', $this->instance->get('url'));

        $this->params['format'] = $this->instance->get('format');
        if ($this->params['format'] == 'jsonp') {
            $this->params['jsoncallback'] = $this->instance->get('jsonCallBack');
        }

        $mbid = $this->instance->get('mbid');

        if (is_array($mbid)) {
            foreach ($mbid as $id) {
                $x .= '&mbid=' . $id;
            }
        }

        if (is_string($mbid)) {
            $this->params['mbid'] = $this->instance->get('mbid');
        }

        $this->params['batch'] = $this->instance->get('batch');

        $this->set('url', $this->get('url') . '?' . urldecode(http_build_query($this->params)));

        return $this;
    }
}
