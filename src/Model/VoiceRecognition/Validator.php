<?php

namespace App\Model\VoiceRecognition;

use App\Model\VoiceRecognition\LookUp\FingerPrint;
use App\Model\VoiceRecognition\LookUp\TrackId;
use App\Model\VoiceRecognition\Submission;
use App\Model\VoiceRecognition\Submission\Batch;
use App\Model\VoiceRecognition\Submission\Status;
use App\Model\VoiceRecognition\ListByMDID;

if(!defined('BASE_PATH')) exit;
/**
 * \Validator
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 2018
 */
class Validator
{
    /**
     * Validate the $lookUp type
     *
     * @param $lookUp FingerPrint|TrackId
     *
     * @throws Exception
     * @return Mixed
     */
    public static function checkLookUpType($lookUp){
        if(!is_object($lookUp) ||
            !in_array(get_class($lookUp), [FingerPrint::class, TrackId::class])) {
            throw new \Exception(__METHOD__ . "($lookUp): $lookUp provided must be an instance of: " . FingerPrint::class . ' or ' . TrackId::class . '. ' . ucfirst(gettype($lookUp)) . ' given.');
        }else{
            return true;
        }
    }

    /**
     * Validate the $instance type
     *
     * @param $instance \Submission
     *
     * @throws Exception
     * @return Mixed
     */
    public static function checkSubmissionType($instance){
        if(!is_object($instance) ||
            !in_array(get_class($instance), [Submission::class])) {
                throw new Exception(__METHOD__ . "($instance): $instance provided must be an instance of: " . Submission::class . '. ' . ucfirst(gettype($instance)) . ' given.');
        }else{
            return true;
        }
    }

    /**
     * Check the $instance type for batch submission request
     *
     * @param $instance \Batch
     *
     * @throws Exception
     * @return Mixed
     */
    public static function checkSubmissionBatchType($instance){
        if(!is_object($instance) ||
            !in_array(get_class($instance), [Batch::class])) {
            throw new Exception(__METHOD__ . "($instance): $instance provided must be an instance of: " . Batch::class . '. ' . ucfirst(gettype($instance)) . ' given.');
        }else{
            return true;
        }
    }

    /**
     * Validate the $instance type
     *
     * @param $instance \Status
     *
     * @throws Exception
     * @return Mixed
     */
    public static function checkSubmissionStatusType($instance){
        if(!is_object($instance) ||
            !in_array(get_class($instance), [Status::class])) {
            throw new Exception(__METHOD__ . "($instance): $instance provided must be an instance of: " . Status::class . '. ' . ucfirst(gettype($instance)) . ' given.');
        }else{
            return true;
        }
    }

    /**
    * Check the $instance type
    *
    * @param $instance \ListByMDID
    *
    * @throws Exception
    * @return Mixed
    */
    public static function checkListByMBIDStatusType($instance){
        if(!is_object($instance) ||
            !in_array(get_class($instance), [ListByMDID::class])) {
            throw new Exception(__METHOD__ . "($instance): $instance provided must be an instance of: " . ListByMDID::class . '. ' . ucfirst(gettype($instance)) . ' given.');
        }else{
            return true;
        }
    }

}
