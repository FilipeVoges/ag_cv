<?php

namespace App\Model\VoiceRecognition;

use App\Model\Entity\Entity;
use App\Model\VoiceRecognition\Submission\Batch;

if(!defined('BASE_PATH')) exit;
/**
 * \ubmission
 *
 * @package App\Model\VoiceRecognition
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 2018
 */
class Submission extends Entity
{
    /**
     * @var string
     */
    protected $format = 'json';

    /**
     * @var array
     */
    private $formatAllowedValues = array(
        'json',
        'xml'
    );

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var string
     */
    protected $clientVersion = '1.0';

    /**
     * @var int
     */
    protected $wait;

    /**
     * @var string
     */
    protected $user;

    /**
     * @var int
     */
    protected $duration;

    /**
     * @var string
     */
    protected $fingerPrint;

    /**
     * @var int
     */
    protected $bitrate;

    /**
     * @var string
     */
    protected $fileFormat;

    /**
     * @var string
     */
    protected $mbid;

    /**
     * @var string
     */
    protected $track;

    /**
     * @var string
     */
    protected $artist;

    /**
     * @var string
     */
    protected $album;

    /**
     * @var string
     */
    protected $albumArtist;

    /**
     * @var int
     */
    protected $year;

    /**
     * @var int
     */
    protected $trackNo;

    /**
     * @var int
     */
    protected $discNo;

    /**
     * @var Array
     */
    protected $requiredParameters = array(
        'user',
        'duration',
        'fingerPrint',
    );

    /**
     * @var string
     */
    protected $url = 'http://api.acoustid.org/v2/submit';

    /**
     * Construct Class
     *
     * @param $userId String
     * @param $duration Integer|Array
     * @param $fingerPrint String|Array
     */
    public function __construct($userId, $duration, $fingerPrint)
    {
        $this->user = $userId;
        $this->duration = $duration;
        $this->fingerPrint = $fingerPrint;
    }

    /**
     * Set response format
     *
     * @param string $format
     *
     * @throws Exception
     * @return $this
     */
    public function setFormat($format){
        if(!in_array($format, $this->formatAllowedValues)) {
            throw new Exception("Passed $format value is not in list of allowed values. Allowed: " . join(', ', $this->formatAllowedValues));
        }

        $this->format = (string)$format;

        return $this;
    }

    /**
     * Set duration
     *
     * @param $duration Integer|Array
     *
     * @return $this
     */
    public function setDuration($duration)
    {
        if(get_class($this) == Batch::class){
            $this->duration = (array)$duration;
        } else {
            $this->duration = (int)$duration;
        }

        return $this;
    }

    /**
     * Set fingerprint obtained from fpcalc utility
     *
     * @param $fingerPrint String|Array
     *
     * @return $this
     */
    public function setFingerPrint($fingerPrint)
    {
        if(get_class($this) == Batch::class){
            $this->fingerPrint = (array)$fingerPrint;
        } else {
            $this->fingerPrint = (string)$fingerPrint;
        }

        return $this;
    }
}
