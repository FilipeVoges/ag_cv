<?php

namespace App\Model\VoiceRecognition;

use App\Model\VoiceRecognition\Validator;
use App\Model\VoiceRecognition\Request\ListByMBID;
use App\Model\VoiceRecognition\Request\Submission as RequestSubmission;
use App\Model\VoiceRecognition\Request\LookUp as RequestLookUp;
use App\Model\VoiceRecognition\Request\Submission\Batch as RequestSubmissionBatch;
use App\Model\VoiceRecognition\Request\Submission\Status as RequestSubmissionStatus;

if(!defined('BASE_PATH')) exit;
/**
 * \AcoustId
 *
 * Class created for communication with AcoustId's Api
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 2018
 */
class AcoustId
{
    /**
     * API KEY
     *
     * @var string
     */
    protected $apiKey;

    /**
     * Construct Class
     *
     * @param $apiKey String
     */
    public function __construct($apiKey){
       $this->apiKey = $apiKey;
    }

    /**
     * Perform lookup and return response
     *
     * @param $instance FingerPrint|TrackId
     *
     * @throws Exception
     * @return \ResponseInterface
     */
    public function lookUp($instance){

        Validator::checkLookUpType($instance);

        $instance->set('apiKey', $this->apiKey);

        $response = (new RequestLookUp($lookUp))->createRequest()->send();

        return $response
    }

    /**
     * Create new submit request
     *
     * @param $instance \Submission
     *
     * @throws Exception
     * @return \ResponseInterface
     */
    public function submission($instance){

        Validator::checkSubmissionType($instance);

        $instance->set('apiKey', $this->apiKey);

        $response = (new RequestSubmission($instance))->createRequest()->send();

        return $response;
    }

    /**
     * @param $instance \Batch
     *
     * @throws Exception
     * @return \ResponseInterface
     */
    public function submissionBatch($instance)
    {
        Validator::checkSubmissionBatchType($instance);

        $instance->set('apiKey', $this->apiKey);

        $response = (new RequestSubmissionBatch($instance))->createRequest()->send();

        return $response;
    }

    /**
     * @param $instance \Status
     *
     * @throws Exception
     * @return \ResponseInterface
     */
    public function submissionStatus($instance)
    {
        Validator::checkSubmissionStatusType($instance);

        $instance->set('apiKey', $this->apiKey);

        $response = (new RequestSubmissionStatus($instance))->createRequest()->send();

        return $response;
    }

    /**
     * Create list request
     *
     * @param $instance \ListByMDID
     *
     * @throws Exception
     * @return \ResponseInterface
     */
    public function listByMBID($instance){

        Validator::checkListByMBIDStatusType($instance);

        $instance->set('apiKey', $this->apiKey);

        $instance->checkRequiredParameters(['url']);

        $response = (new ListByMBID($instance))->createRequest()->send();

        return $response;
    }

}
