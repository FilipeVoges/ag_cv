<?php

namespace App\Model\VoiceRecognition;

use App\Model\Entity\Entity;
use App\Model\VoiceRecognition\LookUp\FingerPrint;
use App\Model\VoiceRecognition\LookUp\TrackId;
use App\Model\VoiceRecognition\Submission\Batch;
use App\Model\VoiceRecognition\Submission\Status;
use GuzzleHttp\Client;

if(!defined('BASE_PATH')) exit;
/**
 * Class Request
 *
 * @package App\Model\VoiceRecognition
 *
 * @author Filipe Voges <filipe.vogesh@gmail.com>
 * @since 2018
 */
abstract class Request extends Entity
{
    /**
     * @var FingerPrint|TrackId|Submission|Status|ListByMDID|Batch
     */
    protected $instance;

    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    protected $params;

    /**
     * Constructor Class
     *
     * @param $instance FingerPrint|TrackId|Submission|Status|ListByMDID|Batch
     */
    public function __construct($instance){
        $this->instance = $instance;
    }

    /**
     * Create request based on $lookUp type
     */
    abstract public function createRequest();

    /**
     * Send a request
     *
     * @return \ResponseInterface
     */
    public function send(){
        return (new Client())->get($this->url);
    }
}
