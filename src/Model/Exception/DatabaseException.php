<?php

namespace App\Model\Exception;

use App\Model\Exception\WaterlooException;

if(!defined('BASE_PATH')) exit;

/**
 * Class responsible for dealing with database-related exceptions
 *
 * @package App\Model\Exception
 * @since 0.1
 * @author Filipe Voges <filipe.voges@vapps.com.br>
 */
class DatabaseException extends WaterlooException{

    /**
     * @var String
     */
    protected $sql;

    /**
     * Construct class
     *
     * @param $message | String
     * @param $code | Integer
     */
    public function __construct($message = null, $code = 0, $sql){
        $this->sql = $sql;
        parent::__construct($message, $code);
    }
}
