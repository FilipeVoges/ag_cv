<?php

namespace App\Model\Exception;

use App\Model\Exception\WaterlooException;

if(!defined('BASE_PATH')) exit;

/**
 * Class responsible for dealing with Exceptions caused by system resources
 *
 * @package App\Model\Exception
 * @since 0.1
 * @author Filipe Voges <filipe.voges@vapps.com.br>
 */
class ResourceException extends WaterlooException{

    /**
     * @var String
     */
    protected $resource;

    /**
     * Construct class
     *
     * @param $message | String
     * @param $code | Integer
     */
    public function __construct($message = '', $code = 500, $resource = 'Entity'){
        $this->resource = $resource;
        parent::__construct($message, $code);
    }

}
