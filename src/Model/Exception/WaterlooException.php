<?php

namespace App\Model\Exception;

use App\Model\Lang\Language;

if(!defined('BASE_PATH')) exit;

/**
 * Class responsible for overall management of exceptions
 *
 * @package App\Model\Exception
 * @since 0.1
 * @author Filipe Voges <filipe.voges@vapps.com.br>
 */
class WaterlooException extends \Exception{

    /**
     * @var boolean
     */
	protected $backTrace = true;

    /**
     * Construct class
     *
     * @param $message | String
     * @param $code | Integer
     */
    public function __construct($message = null, $code = 500){
        if(checktype('NUL', $message)){
            $message = 'Erro Interno'
        }
        parent::__construct($message, $code);
    }
}
