<?php

namespace App\Model;

use App\Model\Entity\Population;
use App\Model\Entity\Tour;

if(!defined('BASE_PATH')) exit;
/**
 * GA
 *
 * @package App\Model
 * @since 0.1
 * @author Filipe Voges <filipe.voges@vapps.com.br>
 */
class GA{

    /**
     * @var Float
     */
    private static $mutationRate = 0.0015;

    /**
     * @var Integer
     */
    private static $tournamentSize = 5;

    /**
     * @var Boolean
     */
    private static $elitism = true;

    /**
     * @var Integer
     */
    private static $mutations = 0;

    /**
     * Evolve one population to the next generation
     *
     * @param $pop \Population
     * @return \Population
     */
    public static function evolvePopulation($pop) {
        $newPopulation = new Population($pop->populationSize(), false);

        $elitismOffset = 0;
        if(self::$elitism){
            $newPopulation->saveTour(0, $pop->getFittest());
            $elitismOffset = 1;
        }

        for($i = $elitismOffset; $i < $newPopulation->populationSize(); $i++) {

            $parent1 = GA::tournamentSelection($pop);
            $parent2 = GA::tournamentSelection($pop);

            $child = GA::crossover($parent1, $parent2);

            $newPopulation->saveTour($i, $child);
        }

        for($i = $elitismOffset; $i < $newPopulation->populationSize(); $i++) {
            GA::mutate($newPopulation->getTour($i));
        }
        return $newPopulation;
    }

    /**
     * Applies the crossover process to a set of country and generates a daughter route
     *
     * @param $parent1 \Tour
     * @param $parent2 \Tour
     * @return \Tour
     */
    public static function crossover(Tour $parent1, Tour $parent2) {

        $child = new Tour();

        $startPos = (int)(random() * $parent1->size());
        $endPos = (int)(random() * $parent1->size());

        for($i = 0; $i < $child->size(); $i++) {

            if($startPos < $endPos && $i > $startPos && $i < $endPos) {
                $child->setCity($i, $parent1->getCity($i));
            }else if($startPos > $endPos) {
                if(!($i < $startPos && $i > $endPos)) {
                    $child->setCity($i, $parent1->getCity($i));
                }
            }
        }

        for($i = 0; $i < $parent2->size(); $i++) {
            if(!$child->containsCity($parent2->getCity($i))) {
                for($z = 0; $z < $child->size(); $z++) {
                    if(is_null($child->getCity($z))) {
                        $child->setCity($z, $parent2->getCity($i));
                        break;
                    }
                }
            }
        }

        return $child;
    }

    /**
     * Mutate a route using the exchange mutation method
     *
     * @param $tour \Tour
     * @return Void
     */
    private static function mutate(Tour $tour) {
        for($tourPos1 = 0; $tourPos1 < $tour->size(); $tourPos1++){
            if(random() < self::$mutationRate){
            	self::$mutations++;
                $tourPos2 = (int)($tour->size() * random());

                $city1 = $tour->getCity($tourPos1);
                $city2 = $tour->getCity($tourPos2);

                $tour->setCity($tourPos2, $city1);
                $tour->setCity($tourPos1, $city2);
            }
        }
    }

    /**
     * In the tournament is selected the best route within a sub-set of random routes of the population.
     * This route will later be used in the crossover process
     *
     * @param $pop \Population
     * @return \Tour
     */
    private static function tournamentSelection($pop) {

        $tournament = new Population(self::$tournamentSize, false);

        for($i = 0; $i < self::$tournamentSize; $i++) {
            $randomId = (int)(random() * $pop->populationSize());
            $tournament->saveTour($i, $pop->getTour($randomId));
        }

        $fittest = $tournament->getFittest();
        return $fittest;
    }


    /**
     * Getter
     *
     * @return Integer
     */
    public static function getMutations(){
    	return self::$mutations;
    }
}
